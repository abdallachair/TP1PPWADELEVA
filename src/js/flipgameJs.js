var jsonhttp;
    if(window.XMLHttpRequest){
        jsonhttp = new XMLHttpRequest();
    } else{
        jsonhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
		
    jsonhttp.open("GET", "src/js/images.json", true);
    jsonhttp.send();	

function startClick(){
    var self = this;
    this.card1 = "";
    this.card2 = "";
    this.card1flipped = false;
    this.card2flipped = false;
    this.flippedCards = 0;
    var json = JSON.parse(jsonhttp.responseText);
    json.imgs = shuffle(json.imgs);
    
    var table = "<table>";
    var counter = 16;
    for (i = 0; i <4; i++) { 
		table += '<tr><td><div class="squares" id="' + json.imgs[counter-1].id +'" > ' +
		'<span class="outside"></span>'+ '<span class="inside"> ' 
        + '<img class="content" src=" ' + json.imgs[counter-1].src +  ' " alt="Card"/></span>' +
		'</div></td><td><div class="squares" id="' + json.imgs[counter-2].id +'" > ' +
		'<span class="outside"></span>'+ '<span class="inside"> ' 
        + '<img class="content" src=" ' + json.imgs[counter-2].src +  ' " alt="Card"/></span>' +
		'</div></td><td><div class="squares" id="' + json.imgs[counter-3].id +'" > ' +
		'<span class="outside"></span>'+ '<span class="inside"> ' 
        + '<img class="content" src=" ' + json.imgs[counter-3].src +  ' " alt="Card"/></span>' +
		'</div></td><td><div class="squares" id="' + json.imgs[counter-4].id +'" > ' +
		'<span class="outside"></span>'+ '<span class="inside"> ' 
        + '<img class="content" src=" ' + json.imgs[counter-4].src +  ' " alt="Card"/></span>' +
		'</div></td></tr>';
        
        counter -= 4;
	  }
    table += "</table>";
     document.getElementById("mainGame").innerHTML = table;
    
    function reset(){
        this.card1 = "";
        this.card2 = "";
        this.card1flipped = false;
        this.card2flipped = false;
    }
    
    function check(){
        if(this.card1.id == this.card2.id){
            reset();
            this.flippedCards += 2;
            if(this.flippedCards === 16){
                alert("you won!!!");
            }
        } else{
            window.setTimeout( function(){
            this.card1.classList.remove("flipped");
            this.card2.classList.remove("flipped");
            reset();
            }, 600 );
        }
    }
    
    var cards = document.querySelectorAll(".squares");
    for (var i = 0, len = cards.length; i < len; i++) {
        var card = cards[i];
        flip(card);
    };
    
    function flip(card){
        var self = this;
        card.addEventListener("click", function(){
             if (!this.classList.contains("flipped")) {
                if (self.card1flipped === false && self.card2flipped === false) {
                    this.classList.add("flipped");
                    self.card1 = this;
                    self.card1flipped = true;
                } else if( self.card1flipped === true && self.card2flipped === false ) {
                    this.classList.add("flipped");
                    self.card2 = this;
                    self.card2flipped = true;
                    check();
                }
             }    
        });
    }
}



function shuffle(array){
    var counter = array.length, temp, index;
    
    while (counter > 0) {
        
    index = parseInt(Math.random() * counter);
        
    counter--;
    
    temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
    }
    return array;
}

function getName(){
    var input = document.getElementById("inputName").value;
    
    document.getElementById("playerName").innerHTML += input + "<br>";
}